import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//pages
import Homepage from "./pages/homepage";
import PrivacyPolicy from "./pages/privacypolicy";
import Confirmation from "./pages/confirmation";
import PageNotFound from "./pages/404";

//components
import Navbar from "./components/navbar";
import Footer from "./components/footer";
import ScrollToTop from "./components/scrolltotop";

function App() {
  return (
    <Router>
      <ScrollToTop />
      <div className="d-flex flex-col just-cont-center align-items-center">
        <Navbar />
        <div className="page-container">
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Route exact path="/privacy" component={PrivacyPolicy} />
            <Route exact path="/confirmation" component={Confirmation} />
            <Route component={PageNotFound} />
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
