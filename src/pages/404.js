import React from "react";

const PageNotFound = () => {
  return (
    <div className="d-flex flex-col width-100 percent height-90-vh just-cont-center align-items-center">
      <h1 className="d-flex font-fam-open-sans font-size-page-not-found-header color-secondary margin-page-not-found-header">
        404 - ikke fundet
      </h1>
      <span className="d-flex font-fam-montserrat font-size-regular margin-bottom-regular margin-page-not-found-span">
        Siden du leder efter kan ikke findes på denne server.
      </span>
      <a className="d-flex font-fam-montserrat font-size-regular" href="/">
        <button
          className="font-fam-open-sans font-size-regular font-weight-b color-font-secondary
            bg-color-primary-button border-style-none border-radius-button-and-input padding-button-primary box-shadow-button cursor-pointer"
        >
          Gå til startside
        </button>
      </a>
    </div>
  );
};

export default PageNotFound;
