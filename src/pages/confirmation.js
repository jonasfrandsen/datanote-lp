import React from "react";
import { Link } from "react-router-dom";

const Confirmation = () => {
  return (
    <div
      className="d-flex flex-col"
      style={{ height: "50vh", marginTop: "3rem" }}
    >
      <h1 className="font-fam-open-sans">Tak for jeres interesse!</h1>
      <span className="font-fam-montserrat margin-bottom-regular">
        Vi tager kontakt til jer så snart som muligt.
      </span>
      <Link to="/" className="margin-top-regular">
        <button
          className="font-fam-open-sans font-size-regular font-weight-b color-font-secondary
            bg-color-primary-button border-style-none border-radius-button-and-input padding-button-primary box-shadow-button cursor-pointer"
        >
          Gå tilbage til forsiden
        </button>
      </Link>
    </div>
  );
};

export default Confirmation;
