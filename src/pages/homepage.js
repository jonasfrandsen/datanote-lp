import React from "react";

//components
import Hero from "../components/homepage/hero";
import BenefitsOne from "../components/homepage/benefits1/benefits1";
import Benefit from "../components/homepage/benefits1/benefit";
import BenefitsTwo from "../components/homepage/benefits2/benefits2";
import CallToAction from "../components/homepage/calltoaction";

//assets
import BenefitsOneImgOne from "../assets/flame-sign-in.png";
import BenefitsOneImgTwo from "../assets/flame-1069.png";
import BenefitsOneImgThree from "../assets/flame-780.png";

/* BENEFITS ONE */
const BeneFitsOneHeader = "En nem løsning til at få den bedste træningsdata";
const BeneFitsTwoHeader = "Automation med et menneskeligt touch";

const Homepage = () => {
  return (
    <div className="d-flex flex-col">
      <Hero />
      <BenefitsOne header={BeneFitsOneHeader}>
        <Benefit
          img={BenefitsOneImgOne}
          alt="benefit one"
          header="Kvalitet træningsdata"
          paragraph="Omhyggeligt annoteret træningsdata er et afgørende element i arbejdet
        med supervised maskinlæring. Vores platform gør det nemt for jer at få
        træningsdata i høj kvalitet."
        />
        <Benefit
          img={BenefitsOneImgTwo}
          alt="benefit two"
          header="Datasikkerhed uden kompromis"
          paragraph="Kryptering af datalagring via industristandard TLS / SSL, der beskytter dine data mod uautoriseret adgang og datakorruption."
        />
        <Benefit
          img={BenefitsOneImgThree}
          alt="benefit three"
          header="En dygtig workforce"
          paragraph="Vi specialiserer i at optræne en arbejdsstyrke til at udføre
          annotering af data for virksomheder og organisationer, der anvender
          maskinlærings-algoritmer til at udvikle kunstig intelligens."
        />
      </BenefitsOne>
      <BenefitsTwo
        header={BeneFitsTwoHeader}
        paragraphOne="Datanote blev grundlagt på baggrund af et ønske om at give arbejdsmuligheder til dem, der har mest brug for det. 
        Som kunde hos Datanote hjælper I mennesker, der står uden for det konventionelle arbejdsmarked. Det kan være mennesker, 
        der på grund af fysiske handicap eller psykiske sårbarheder ikke passer ind i et almindeligt 8-16 job, men som ellers er arbejdsdygtige, hvis der tages hensyn til deres betingelser."
        paragraphTwo="Vi har en arbejdsstyrke på mere end 250 personer, som arbejder med at give liv til nogle af de mest spændende applikationer inden for kunstig intelligens.
        Samtlige annotører har gennemgået et specialiseret træningsforløb for at forberede dem bedst muligt til arbejdet som professionelle annotører."
      />
      <CallToAction header="Kontakt os og hør hvordan vi kan hjælpe jer" />
    </div>
  );
};

export default Homepage;
