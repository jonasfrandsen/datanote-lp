import React from "react";
import { Link } from "react-router-dom";

//assets
import ArrowBackIcon from "../assets/arrow_back-24px.svg";

const PrivacyPolicy = () => {
  return (
    <div className="line-height-privacy-policy" style={{ marginTop: "3rem" }}>
      <Link
        to="/"
        className="d-flex align-items-center font-fam-montserrat font-weight-b color-primary margin-top-regular margin-bottom-regular"
      >
        <img
          src={ArrowBackIcon}
          alt="arrow back icon"
          className="margin-right-regular"
        />{" "}
        Tilbage til forsiden
      </Link>
      <h1 className="font-fam-open-sans">Privatlivspolitik</h1>
      <p className="font-fam-montserrat">
        Vi tager databeskyttelse seriøst. Vi behandler persondata og har derfor
        udarbejdet denne privatlivs- og cookiepolitik, der informerer dig om,
        hvordan dine personlige data bliver behandlet.
      </p>
      <p className="font-fam-montserrat">
        Botlab er databehandler af data, vi indsamler om dig, og vi sikrer, at
        dine personlige data bliver behandlet i overensstemmelse med loven.
      </p>
      <p className="font-fam-montserrat">
        Hvis du ønsker at høre mere om vores behandling af persondata, er du
        altid velkommen til at kontakte os på: datanote@outlook.dk eller på +45
        5278 9852.
      </p>
      <br />
      <h2 className="font-fam-open-sans">Behandling af personlig data</h2>
      <p className="font-fam-montserrat">
        Personoplysninger er alle slags informationer, der i et eller andet
        omfang kan henføres til dig. Når du benytter vores website, indsamler og
        behandler vi en række af sådanne informationer. Det sker f.eks. ved
        almindelig tilgang til indhold, hvis du kontakter os via vores
        kontaktformular, eller hvis sender os en ansøgning via vores
        ansøgningsformular.
      </p>
      <p className="font-fam-montserrat">
        Vi indsamler og behandler typisk følgende typer af oplysninger: Et unikt
        ID og tekniske oplysninger om din computer, tablet eller mobiltelefon,
        dit IP-nummer, geografisk placering, samt hvilke sider du klikker på. I
        det omfang, du selv indtaster informationerne i forbindelse med
        henvendelse i vores kontaktformular, behandles desuden navn og e-mail.
      </p>
      <br />
      <h2 className="font-fam-open-sans">Formål</h2>
      <p className="font-fam-montserrat">
        Oplysningerne bruges til at identificere dig som bruger samt til at
        optimere vores services og indhold. Desuden anvendes oplysningerne for
        at kunne levere de services, du har efterspurgt. Disse services kan
        enten være som følge af en forespørgsel i kontaktformularen eller som
        følge af en ansøgning. Grundet dette formål forbeholder vi os retten til
        at kontakte dig via enten e-mail eller telefonnummer, efter du har
        rettet henvendelse til os, såfremt vi finder dette nødvendigt.
      </p>
      <br />
      <h2 className="font-fam-open-sans">Dine rettigheder</h2>
      <p className="font-fam-montserrat">
        Du har ret til at få oplyst, hvilke personoplysninger, vi behandler om
        dig. Du kan desuden til enhver tid gøre indsigelse mod, at oplysninger
        anvendes. Du kan også tilbagekalde dit samtykke til, at der bliver
        behandlet oplysninger om dig. Hvis de oplysninger, der behandles om dig,
        er forkerte, har du ret til, at de bliver rettet eller slettet.
        Henvendelse herom kan ske til datanote@outlook.dk eller +45 5278 9852.
        Hvis du vil klage over vores behandling af dine personoplysninger, har
        du også mulighed for at tage kontakt til Datatilsynet.
      </p>
      <br />
      <div className="d-flex flex-col" style={{ marginBottom: "2rem" }}>
        <h2 className="font-fam-open-sans">Udgiver</h2>
        <span className="font-fam-montserrat">
          Websitet ejes og publiceres af:
        </span>
        <span className="font-fam-montserrat">Datanote</span>
        <span className="font-fam-montserrat">Nørrebrogade 44</span>
        <span className="font-fam-montserrat">8000 Aarhus C.</span>
        <span className="font-fam-montserrat">Telefon: +45 52789852</span>
        <span className="font-fam-montserrat margin-bottom-regular">
          E-mail: datanote@outlook.dk
        </span>
        <Link
          to="/"
          className="d-flex align-items-center font-fam-montserrat font-weight-b color-primary margin-top-regular margin-bottom-regular"
        >
          <img
            src={ArrowBackIcon}
            alt="arrow back icon"
            className="margin-right-regular"
          />{" "}
          Tilbage til forsiden
        </Link>
      </div>
    </div>
  );
};

export default PrivacyPolicy;
