import React from "react";

//components
import PrimaryButton from "../elements/primarybutton";

//assets
import HeroImg from "../../assets/hero-image.png";
import ScrollDownIcon from "../../assets/expand_more-24px.svg";

const Hero = () => {
  return (
    <section className="d-flex flex-col width-100-percent height-90-vh height-auto-s just-cont-flex-end margin-section-xl">
      <div
        className="d-flex flex-direction-mobile-col width-100-percent align-items-center just-cont-space-between"
        style={{ height: "100%" }}
      >
        <div className="d-flex flex-col width-46-percent width-hero-container-s just-cont-center align-items-flex-start">
          <h1 className="d-flex font-fam-open-sans font-size-hero-header font-size-hero-header-s font-weight-b line-height-hero-header margin-hero-header">
            Annotering af data hurtigt, nemt og kvalitetsbevidst
          </h1>
          <p className="d-flex font-fam-montserrat font-size-hero-paragraph-xl font-size-hero-paragraph-s line-height-hero-paragraph-xl line-height-hero-paragraph-s margin-hero-paragraph">
            Få træningsdata i høj kvalitet gennem vores dygtige og dedikerede
            workforce, samtidig med at I hjælper mennesker uden for
            abejdsmarkedet.
          </p>
          <a href="#contactform" className="d-flex align align-self-strech-s ">
            <PrimaryButton type="button" label="Kontakt os om et tilbud" />
          </a>
        </div>
        <div
          className="d-flex width-50-percent width-hero-container-s margin-hero-image-container-s"
          style={{
            backgroundImage: `url(${HeroImg})`,
            height: "0",
            padding: "0 0 63.3% 0",
            backgroundPosition: "center center",
            backgroundSize: "100%",
            backgroundRepeat: "no-repeat",
          }}
        >
          {/*  <img
            className="width-100-percent"
            src={}
            alt="annotation tool"
          /> */}
        </div>
      </div>
      <div className="d-flex flex-col align-items-center">
        <a href="#benefitsone">
          <button className="d-flex flex-col align-items-center border-style-none bg-transparent cursor-pointer">
            <span className="d-flex font-fam-open-sans font-size-regular font-weight-b">
              Lær mere
            </span>
            <img src={ScrollDownIcon} alt="scroll down icon" />
          </button>
        </a>
      </div>
    </section>
  );
};

export default Hero;
