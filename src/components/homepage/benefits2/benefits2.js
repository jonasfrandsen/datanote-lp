import React from "react";

const BenefitsTwo = ({ header, paragraphOne, paragraphTwo }) => {
  return (
    <section className="d-flex flex-col width-100-percent align-items-center margin-section-xl">
      <h1 className="d-flex font-fam-open-sans font-size-section-header line-height-section-header-s text-align-center text-align-left-s margin-section-header-xl">
        {header}
      </h1>
      <p className="d-flex flex-col align-items-center font-fam-montserrat font-size-benefit-paragraph-xl line-height-benefits-paragraph-xl text-align-center text-align-left-s margin-benefits-paragraph">
        {paragraphOne}
        <br />
        <br />
        {paragraphTwo}
      </p>
    </section>
  );
};

export default BenefitsTwo;
