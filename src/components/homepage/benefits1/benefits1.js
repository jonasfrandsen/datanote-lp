import React from "react";

const BenefitsOne = ({ header, children }) => {
  return (
    <section
      id="benefitsone"
      className="d-flex flex-col align-items-center-m width-100-percent margin-section-xl"
    >
      <h1 className="d-flex just-cont-center font-fam-open-sans font-size-section-header line-height-section-header-s text-align-center text-align-left-s margin-section-header-xl">
        {header}
      </h1>
      <div className="d-flex flex-direction-tablet-col flex-direction-mobile-col just-cont-space-around">
        {children}
      </div>
    </section>
  );
};

export default BenefitsOne;
