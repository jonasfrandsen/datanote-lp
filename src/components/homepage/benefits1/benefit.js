import React from "react";

const Benefit = ({ img, alt, header, paragraph }) => {
  return (
    <div className="d-flex flex-col align-items-center-m align-items-flex-start-s width-benefits-container">
      <div style={{ height: "260px" }} className="margin-benefits-image-xl">
        <img
          className="width-benefits-img-container height-auto"
          src={img}
          alt={alt}
        />
      </div>
      <h2 className="d-flex font-fam-open-sans font-size-benefit-header font-weight-b text-align-center-m text-align-left-s margin-benefits-header-xl">
        {header}
      </h2>
      <p className="d-flex font-fam-montserrat font-size-benefit-paragraph-xl line-height-benefits-paragraph-xl text-align-center-m text-align-left-s margin-benefits-paragraph">
        {paragraph}
      </p>
    </div>
  );
};

export default Benefit;
