import React, { useState } from "react";

//components
import InputField from "../elements/inputfield";
import PrimaryButton from "../elements/primarybutton";
import TextArea from "../elements/textarea";

const ctaListOneElements = [
  "Fast kontaktperson",
  "Projektstyring",
  "24/7 Support",
  "NDA",
];
const ctaListTwoElements = [
  "Fast projektpris",
  "Tilpasset afrapportering",
  "Hurtig eksekvering",
  "Sparring med AI-konsulenter",
];

const CallToAction = ({ header }) => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [company, setCompany] = useState("");
  const [description, setDescription] = useState("");

  const ctaListOne = ctaListOneElements.map((elem, index) => (
    <li
      key={`cta-list-one-elements-${index}`}
      className="font-fam-montserrat font-size-regular font-weight-b line-height-cta-list-item-s color-secondary list-style-type-none padding-cta-list-item-xl padding-cta-list-item-s"
    >
      {elem}
    </li>
  ));

  const ctaListTwo = ctaListTwoElements.map((elem, index) => (
    <li
      key={`cta-list-two-elements-${index}`}
      className="font-fam-montserrat font-size-regular font-weight-b line-height-cta-list-item-s color-secondary list-style-type-none padding-cta-list-item-xl padding-cta-list-item-s"
    >
      {elem}
    </li>
  ));

  console.log(fullName);
  console.log(email);
  console.log(phone);
  console.log(company);
  console.log(description);

  return (
    <section className="d-flex flex-col width-100-percent align-items-center margin-section-xl">
      <h1 className="d-flex font-fam-open-sans font-size-section-header line-height-section-header-s text-align-center text-align-left-s margin-section-header-xl">
        {header}
      </h1>
      <div className="d-flex flex-col width-cta-container-s bg-color-secondary padding-cta-container-xl border-radius-cta-container border-radius-cta-container-s box-shadow-button">
        <div className="d-flex flex-direction-mobile-col cta-element-flex-direction margin-cta-list-container margin-cta-list-container-s">
          <div className="d-flex margin-cta-list-s ">
            <ul className="d-flex flex-col margin-0 padding-0">{ctaListOne}</ul>
          </div>
          <div className="d-flex">
            <ul className="d-flex flex-col margin-0 padding-0">{ctaListTwo}</ul>
          </div>
        </div>
        <div id="contactform" className="d-flex flex-col">
          <span
            className="d-flex font-fam-montserrat font-size-regular line-height-cta-span-s margin-left-regular"
            style={{ marginBottom: "8px" }}
          >
            Udfyld kontaktformularen og vi vender tilbage hurtigst muligt.
          </span>
          <span className="d-flex flex-direction-mobile-col font-fam-montserrat font-size-regular line-height-cta-span-s margin-left-regular">
            Eller ring direkte på tlf.&nbsp;
            <a
              href="tel:+4552789852"
              className="d-flex font-fam-montserrat font-size-regular font-weight-b color-font-primary"
            >
              +45 52 78 98 52
            </a>
          </span>
          <form
            action="https://outlook.us7.list-manage.com/subscribe/post"
            method="POST"
            className="d-flex flex-col align-items-center align-items-flex-start-s margin-form"
          >
            <input type="hidden" name="u" value="1724c1e7a34087b1c32dfd2d5" />
            <input type="hidden" name="id" value="3a735d7418" />
            <div className="d-flex flex-col cta-element-flex-direction width-100-percent width-cta-container-s">
              <div className="d-flex flex-direction-mobile-col width-100-percent just-cont-space-between width-cta-container-s margin-input-inner-container-m">
                <InputField
                  type="text"
                  value={fullName}
                  name="MERGE1"
                  id="MERGE1"
                  placeholder="Navn (påkrævet)"
                  onChange={(e) => setFullName(e.target.value)}
                  required={true}
                />
                <InputField
                  type="email"
                  value={email}
                  name="MERGE0"
                  id="MERGE0"
                  placeholder="Email (påkrævet)"
                  onChange={(e) => setEmail(e.target.value)}
                  required={true}
                />
              </div>
              <div className="d-flex flex-direction-mobile-col just-cont-space-between width-cta-container-s margin-input-inner-container-m">
                <InputField
                  type="tel"
                  value={phone}
                  name="MERGE4"
                  id="MERGE4"
                  placeholder="Telefon"
                  onChange={(e) => setPhone(e.target.value)}
                  required={false}
                />
                <InputField
                  type="text"
                  value={company}
                  name="MERGE2"
                  id="MERGE2"
                  placeholder="Virksomhed"
                  onChange={(e) => setCompany(e.target.value)}
                  required={false}
                />
              </div>
            </div>
            <div className="d-flex width-cta-container-s width-100-percent just-cont-center">
              <TextArea
                type="text"
                value={description}
                name="MERGE3"
                id="MERGE3"
                placeholder="Hvad kan vi hjælpe med?"
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
            <PrimaryButton type="submit" label="Kontakt os" />
          </form>
        </div>
      </div>
    </section>
  );
};

export default CallToAction;
