import React from "react";

const Navbar = () => {
  return (
    <div
      className="d-flex align-items-flex-end navbar-container"
      style={{ height: "10vh" }}
    >
      <div className="d-flex width-100-percent just-cont-space-between align-items-center">
        <div>
          <a href="/">
            <h1 className="d-flex font-fam-open-sans font-size-nav-header font-weight-b color-font-primary margin-0">
              Datanote
            </h1>
          </a>
        </div>
        {/* <div className="d-flex">
          <a
            href="/"
            className="font-fam-open-sans font-size-nav-link-xl font-weight-b margin-nav-link-xl "
          >
            <button
              className="font-fam-open-sans font-size-regular font-weight-b color-primary 
            bg-color-secondary-button border-color-secondary-button border-style-solid border-radius-button-and-input padding-button-secondary box-shadow-button cursor-pointer"
            >
              Kontakt
            </button>
          </a>
        </div> */}
      </div>
    </div>
  );
};

export default Navbar;
