import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <div className="d-flex flex-direction-mobile-col just-cont-space-between align-items-center align-items-flex-start-s bg-color-secondary footer-container">
      <div className="d-flex flex-direction-tablet-col flex-direction-mobile-col align-items-center align-items-flex-start-m">
        <div className="d-flex flex-col margin-footer-inner-container">
          <h1 className="d-flex font-fam-open-sans font-size-footer-header-xl font-size-b color-secondary margin-0">
            Datanote
          </h1>
          <span className="d-flex font-fam-open-sans font-size-regular font-weight-b color-font-footer-copyright line-height-footer-span-xl">
            Copyright 2020-2021
          </span>
          <span
            className="font-fam-montserrat margin-top-regular d-none-m"
            style={{ color: "#929292" }}
          >
            Illustrations by{" "}
            <a
              href="https://icons8.com/"
              className="font-fam-montserrat font-weight-b"
              style={{ color: "#929292" }}
            >
              Icons8
            </a>
          </span>
        </div>
        <div className="d-flex flex-col margin-footer-inner-container">
          <span className="d-flex font-fam-open-sans font-size-regular font-weight-b line-height-footer-span-xl">
            Nørrebrogade 44 <br />
            8000 Århus C
          </span>
          <span className="font-fam-open-sans font-size-regular font-weight-b color-font-footer-copyright line-height-footer-span-xl">
            <a
              href="tel:+4552789852"
              className="d-flex font-fam-open-sans font-size-regular font-weight-b color-font-primary"
            >
              +45 52 78 98 52
            </a>
          </span>
          <span
            className="d-flex-m font-fam-montserrat margin-top-regular d-none"
            style={{ color: "#929292" }}
          >
            Illustrations by&nbsp;
            <a
              href="https://icons8.com/"
              target="_blank"
              rel="noopener noreferrer"
              className="font-fam-montserrat font-weight-b"
              style={{ color: "#929292" }}
            >
              Icons8
            </a>
          </span>
        </div>
      </div>
      <div>
        <Link
          to="/privacy"
          exact
          className="d-flex font-fam-open-sans font-size-regular font-weight-b color-font-primary"
        >
          Privatlivspolitik
        </Link>
      </div>
    </div>
  );
};

export default Footer;
