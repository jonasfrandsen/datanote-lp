import React from "react";

const TextArea = ({ value, name, placeholder, onChange }) => {
  return (
    <textarea
      className="font-fam-montserrat font-size-regular width-100-percent height-cta-textarea-xl border-style-solid border-width-button-and-input 
      border-radius-button-and-input border-color-input padding-cta-input-xl margin-input textarea-overflow-none textarea-no-resize"
      value={value}
      name={name}
      placeholder={placeholder}
      onFocus={(e) => (e.target.placeholder = "")}
      onBlur={(e) => (e.target.placeholder = placeholder)}
      onChange={onChange}
    />
  );
};

export default TextArea;
