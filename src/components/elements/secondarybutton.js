import React from "react";

const SecondaryButton = ({ type, label }) => {
  return (
    <button
      type={type}
      className="font-fam-open-sans font-size-regular font-weight-b bg-color-secondary-button border-color-secondary-button color-secondary padding-button-primary border-style-solid border-width-button-and-input border-radius-button-and-input box-shadow-button cursor-pointer"
    >
      {label}
    </button>
  );
};

export default SecondaryButton;
