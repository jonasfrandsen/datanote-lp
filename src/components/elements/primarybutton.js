import React from "react";

const PrimaryButton = ({ type, onClick, label }) => {
  return (
    <button
      onClick={onClick}
      type={type}
      className="d-flex just-cont-center align-items-center width-button-s font-fam-open-sans font-size-regular font-weight-b bg-color-primary-button color-font-secondary border-radius-button-and-input border-style-none padding-button-primary box-shadow-button cursor-pointer"
    >
      {label}
    </button>
  );
};

export default PrimaryButton;
