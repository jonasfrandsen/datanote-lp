import React from "react";

const InputField = ({
  type,
  name,
  id,
  value,
  placeholder,
  tabindex,
  onChange,
  required,
}) => {
  return (
    <input
      className="font-fam-montserrat font-size-regular width-cta-input width-cta-input-s 
      border-style-solid border-width-button-and-input border-radius-button-and-input border-color-input 
      padding-cta-input-xl margin-input"
      type={type}
      name={name}
      id={id}
      value={value}
      placeholder={placeholder}
      onFocus={(e) => (e.target.placeholder = "")}
      onBlur={(e) => (e.target.placeholder = placeholder)}
      tabindex={tabindex}
      onChange={onChange}
      required={required}
    />
  );
};

export default InputField;
